// App.js
import React from 'react';
import './App.css';
import Header from './Component/Header'
import AutoComplete from './Component/AutoComplete'
import SignIn from './Login'
import SingUp from './Component/SignUp'
import Way from './Component/Way'
import newStreet from './Component/NewStreet'
import Addupdate from './Component/Addupdate'
import UpdateNode from './Component/UpdateNode'
import UpdateEdge from './Component/UdateEdge'
import AddStreet from './Component/AddStreet'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import store from './Redux/Store'
import { Provider } from 'react-redux'
import Logo from './Component/logo'
import Terms from './Component/Terms'

function App() {

  return (
    <Provider store={store}>
    <div className="main-wrapper"><Header/>
    {/* <Router> */}
      <Switch>
      <Route path="/login" component={SignIn}/>
      <Route path="/singUp" component={SingUp}/>
      <Route path="/way" component={AutoComplete}/>
      <Route path="/getUpdate" component={Addupdate}/>
      <Route path="/UpdateNode" component={UpdateNode}/>
      <Route path="/UpdateEdge" component={UpdateEdge}/>
      <Route path="/newStreet" component={newStreet}/>
      <Route path="/terms" component={Terms}/>
      <Route path="/" component={SignIn}/>
      </Switch>
    {/* </Router> */}

    </div>
</Provider>
  );
}

export default App;